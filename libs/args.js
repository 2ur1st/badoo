
var system = require('system');

/**
 * get arg from cli by name
 * @param key
 * @returns {*}
 */
exports.get = function(key) {
    var length = system.args.length;
    if(!key || length < 2) return false;

    for(var i=1; i<length; i++) {
        var regex = '--' + key.trim() + '=[\'\"]?(.+)[\'\"]?';
        var option = system.args[i].match(new RegExp(regex, 'i'));
        if(option && option.length) return option[1];
    }
    return false;
};

/**
 * get all args from cli
 * @returns {*}
 */
exports.all = function() {
    var length = system.args.length;
    if(length < 2) return [];

    var options = {};
    for(var i=1; i<length; i++) {
        var regex = '--(.+)=[\'\"]?(.+)[\'\"]?';
        var attr = system.args[i].match(new RegExp(regex, 'i'));
        if(attr && attr.length) options[attr[1]] = attr[2];
    }
    return options;
};

/**
 * property
 * get args length
 */
exports.length = system.args.length;
