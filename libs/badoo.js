var page = require('webpage').create();

function Badoo(name, password) {

    var login = name;
    var pass = password;
    var url = 'https://badoo.ru';

    this.auth = function() {
        page.open(url).then(function() {
            page.viewportSize =
                { width:650, height:620 };
            page.render('page.png',
                {onlyViewport:true});
        });
    }

}

exports.init = function(name, password) {
    return new Badoo(name, password);
};
